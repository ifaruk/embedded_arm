/*
 * bitwise_hacks.h
 *
 *  Created on: 14 A?u 2014
 *      Author: ifyalciner
 */

#ifndef BITWISE_HACKS_H_
#define BITWISE_HACKS_H_

#include <stdlib.h>
#include <stdint.h>

#ifdef  __cplusplus
extern "C" {
#endif

static inline uint32_t
bitw_32_mask_lowest_1_bit(uint32_t x)
{
	return (x & (-x));
}

static inline uint32_t
bitw_32_mask_highest_1_bit(uint32_t x)
{
	x |= (x >> 1);
	x |= (x >> 2);
	x |= (x >> 4);
	x |= (x >> 8);
	x |= (x >> 16);
	return x - (x >> 1);
}

static inline uint32_t
bitw_32_mask_lowest_0_bit(uint32_t x)
{
	return (~x & (x + (uint32_t)0x1));
}

static inline uint32_t
bitw_32_mask_left_of_lowest_1_bit(uint32_t x)
{
	return (x ^ (-x));
}

static inline uint32_t
bitw_32_mask_right_of_and_lowest_1_bit(uint32_t x)
{
	return (x ^ (x - (uint32_t)0x1));
}

static inline uint32_t
bitw_32_mask_right_of_lowest_1_bit(uint32_t x)
{
	return (~x & (x - (uint32_t)0x1));
}

static inline uint32_t
bitw_32_set_nth_bit(uint32_t x,
                    uint32_t n)
{
	return (x | ((uint32_t)0x1 << n));
}

static inline uint32_t
bitw_32_mask_nth_bit(uint32_t x,
                     uint32_t n)
{
	return (x & ((uint32_t)0x1 << n));
}

static inline uint32_t
bitw_32_get_nth_bit_mask(uint32_t n)
{
	return ((uint32_t)0x1 << n);
}

static inline uint32_t
bitw_32_right_propagate_lowest_1_bit(uint32_t x)
{
	return (x | (x - (uint32_t)0x1));
}

static inline uint32_t
bitw_32_set_lowest_0_bit(uint32_t x)
{
	return (x | (x + (uint32_t)0x1));
}

static inline uint32_t
bitw_32_unset_nth_bit(uint32_t x,
                      uint32_t n)
{
	return (x & ~((uint32_t)0x1 << n));
}

static inline uint32_t
bitw_32_unset_lowest_1_bit(uint32_t x)
{
	return (x & (x - (uint32_t)0x1));
}

static inline uint32_t
bitw_32_toogle_nth_bit(uint32_t x,
                       uint32_t n)
{
	return (x ^ ((uint32_t)0x1 << n));
}

static inline uint32_t
bitw_32_shift_rightmost_lowest_1_bit(uint32_t x)
{
	return (x ^ (x & (-x)));
}

/**
 * If not found a HIGH bit then returns 0
 * @warning LSB has index 1
 */
static inline uint32_t
bitw_32_msb_index(uint32_t x)
{
	static const uint32_t bval[] = {0,
	                                1,
	                                2,
	                                2,
	                                3,
	                                3,
	                                3,
	                                3,
	                                4,
	                                4,
	                                4,
	                                4,
	                                4,
	                                4,
	                                4,
	                                4};
	
	uint32_t r = 0;
	if(x & 0xFFFF0000)
	{
		r += 16 / 1;
		x >>= 16 / 1;
	}
	if(x & 0x0000FF00)
	{
		r += 16 / 2;
		x >>= 16 / 2;
	}
	if(x & 0x000000F0)
	{
		r += 16 / 4;
		x >>= 16 / 4;
	}
	return r + bval[x];
}

static inline uint32_t
bitw_32_create_mask_upto(uint32_t bit_size)
{
	return ((((uint32_t)0x1) << bit_size) - 1);
}

static inline uint32_t 
bitw_32_reverse_bits(uint32_t n) {
	n = ((n&0xffff0000) >> 16) |  ((n&0x0000ffff) << 16);
	n = ((n&0xff00ff00) >> 8) | ((n&0x00ff00ff) << 8);
	n = ((n&0xf0f0f0f0) >> 4) | ((n&0x0f0f0f0f) << 4);
	n = ((n&0xcccccccc) >> 2) | ((n&0x33333333) << 2);
	n = ((n&0xaaaaaaaa) >> 1) | ((n&0x55555555) << 1);

	return n;
}

static inline uint64_t
bitw_64_mask_highest_1_bit(uint64_t x)
{
	x |= (x >> 1);
	x |= (x >> 2);
	x |= (x >> 4);
	x |= (x >> 8);
	x |= (x >> 16);
	x |= (x >> 32);
	return x - (x >> 1);
}

static inline uint64_t
bitw_64_create_mask_upto(uint64_t bit_size)
{
	return ((((uint64_t)0x1) << bit_size) - 1);
}

#ifdef  __cplusplus
}
#endif

#endif /* BITWISE_HACKS_H_ */
