#include <cstdarg>
#include <cstdio>

#define DEBUG_BUFFER_STRING_LEN 200

void
debug_printf(const char* format,
             ...)
{
	using namespace std;
	char    buffer[DEBUG_BUFFER_STRING_LEN];
	va_list args;
	va_start(args,
	         format);
	vsprintf(buffer,
	         format,
	         args);
	va_end(args);
	///Know string is in buffer then send it with any serial data communication channel
}
