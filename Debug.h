#ifndef _DEBUG_H_
#define _DEBUG_H_

#ifdef DEBUG
	#define DEBUG_PRINTF( ...) debug_printf( __VA_ARGS__)
	
	void
	debug_printf( const char* format,
			...);
#else
	#define DEBUG_PRINTF(...)
#endif

#endif /* _DEBUG_H_ */
