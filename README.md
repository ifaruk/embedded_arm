These are my frequently used features while developing C++ applications in embedded systems, especially ARM Cortex-M.

##Bitwise Hacks (bitwise_hacks.h)
Low level memory (register) operations or high performance computations or algorithms can exploit the benefits bitwise 
operations. This is a header only library which is meant to be inlined in your code. It will not only make your code more 
readable (instead of directly using formulas, not because the names I choose are too clear. I think i need to fix that some time),
there are some functions you may find really useful.

#####Related to
* Bitwise AND, OR, NOT, EXOR
* Binary operations
* Two's Complement
* Optimization

##Circular Buffer (circular_buffer.[c,h])
Simple circular buffer implementation.

##Custom Memory Manager (Heap.cpp)
I am frequently using FreeRTOS and don't want two memory managers (__libc__ and __FreeRTOS__) to work on a system. Which makes it 
harder to follow. FreeRTOS memory manager feels easier to tackle for me if a debug occurs. And they claim to provide
more efficient mechanism (?). So you need to override the libc(++) dynamic memory allocation/de-allocation functions 
so that any standard library will also work with FreeRTOS heap.


#####Related to
* Memory Management
* Dynmamic Memory Allocation
* new, delete, malloc, free


##Fault - Error Handler (Error_handler.[c,h])
Debugging can be really hard in embedded systems. Here a __fault handler__ which can be 
called in serious fault situations. This handler stores the previous state of registers
including stack pointer, link register and program counter. So that you can have at least 
an idea about the last point before system failure occurred.
  
#####Related to
 * GNU compiler 
 
#####Related to
 * ARM Cortex-M
 * FreeRTOS
 * Debugging
 * Hardfault handler
 * Stack Pointer (msp, sp)
 * Link Register (lr)
 * Program Counter (pc)
 
#####More on
 * [FreeRTOS: Debugging Hard Fault & Other Exceptions](http://www.freertos.org/Debugging-Hard-Faults-On-Cortex-M-Microcontrollers.html)
 * [feabhas: Developing a Generic Hard Fault handler for ARM Cortex-M3/Cortex-M4](https://blog.feabhas.com/2013/02/developing-a-generic-hard-fault-handler-for-arm-cortex-m3cortex-m4/)
 
##Pure Virtual (pure_virtual.c)
Pure Virtual Methods in Class definition in C++ increases binary size substantially. 
The reason turned out to be related to exception handling mechanism was including a 
bunch of libraries even though you may use `--noexception` during compilation.
Find details in the question [here](http://stackoverflow.com/questions/37946912/declaring-abstract-class-pure-virtual-method-increase-binary-size-substantiall) i asked in Stackoverflow.
The solution is to override the function 

#####Related to
* GNU
* C++
* Object Oriented
* Virtual Methods
* Virtual Class
* Abstract Class
* Bigger Binary Size

#####More on
* [My Question in Stackoverflow](http://stackoverflow.com/questions/37946912/declaring-abstract-class-pure-virtual-method-increase-binary-size-substantiall)
* [A blog post](http://elegantinvention.com/blog/information/smaller-binary-size-with-c-on-baremetal-g/)
* [Another question of same problem](http://stackoverflow.com/questions/14689639/can-i-disable-exceptions-for-when-a-pure-virtual-function-is-called?noredirect=1&lq=1)
* [The culprit in libstdc++](https://gcc.gnu.org/svn/gcc/tags/gcc_4_4_3_release/libstdc++-v3/libsupc++/pure.cc)

##Debug.[cpp,h]
This file introduces functions like printf which can be used in debug printing (as I did). 
Note that I don't override the function printf but introduce an alternative to be used
all throughout the project. Which can be disabled later by #Macros.

#####Requirements
* GNU compiler. (__VA_ARGS__ keyword)

#####Related to
* How to override printf?
* Best way of log mechanism in embedded systems.