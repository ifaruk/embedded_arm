
///formatter:off
#include <FreeRTOS.h>
#include <new>
#include <cstdlib>

// Define the 'new' operator for C++ to use the freeRTOS memory management
// functions. THIS IS NOT OPTIONAL!
//

void*
operator new(std::size_t size)
{
	return pvPortMalloc(size);
}

void*
operator new[](std::size_t size)
{
	return pvPortMalloc(size);
}


void*
operator new(std::size_t size,
             const std::nothrow_t& tag)
{
	return pvPortMalloc(size);
}

void*
operator new[](std::size_t size,
               const std::nothrow_t& tag)
{
	return pvPortMalloc(size);
}

//
// Define the 'delete' operator for C++ to use the freeRTOS memory management
// functions. THIS IS NOT OPTIONAL!
//
void
operator delete(void* p)
{
	vPortFree(p);
	//Additional protection to prevent this address to be used again
	p = NULL;
}

void
operator delete[](void* p)
{
	vPortFree(p);
	//Additional protection to prevent this address to be used again
	p = NULL;
}


void
operator delete(void* p,

                const std::nothrow_t& tag)
{
	vPortFree(p);
	//Additional protection to prevent this address to be used again
	p = NULL;
}

void
operator delete[](void* p,
                  const std::nothrow_t& tag)
{
	vPortFree(p);
	//Additional protection to prevent this address to be used again
	p = NULL;
}


extern "C"
{

void* malloc(size_t size)
{
	return pvPortMalloc(size);
}

void free(void* p)
{
	vPortFree(p);
	//Additional protection to prevent this address to be used again
	p = NULL;
}

}
///formatter:on

