//This function is overridden to prevent pure_virtual exception to occur
//and increase the size of binary
void __cxa_pure_virtual(void)
{
	__asm("BKPT #0\n");
}